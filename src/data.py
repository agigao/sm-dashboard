from pandas import DataFrame, to_datetime


# loading just required functionality of Panda, saving bites of RAM/Time.
def to_dataframe(data=None, columns=None):
    """
    Convert SQL result to a dataframe, make proper conversion on the way as well.
    Parse datetime column
    :param data: ResultProxy reference.
    :param columns: list of columns
    :return dataframe: object reference
    """
    df = DataFrame(data, columns=columns, dtype=float)
    for i, r in df.iterrows():
        if r.PRODUCTION == 'N':
            df.at[i, 'VERSION_NAME'] = f'[DEV] {r.VERSION_NAME}'
    df.set_index(['MODEL_TYPE', 'MODEL_NAME'], inplace=True)
    df['PERIOD'] = to_datetime(df['PERIOD'])
    df.sort_values(by=['MODEL_TYPE', 'MODEL_NAME', 'TARGET_TYPE', 'VERSION_NAME', 'PERIOD'],
                   inplace=True)
    return df


def get_category(data, category):
    """
    Filter dataframe according the model categories.
    :param data: Pandas DataFrame
    :param category: "App", "Beh" or "Prop"
    :return: new Dataframe for the particular category
    """
    # Not used much. At least for now.
    return (data.copy()
            .loc(category)
            .reset_index(drop=True, level='MODEL_TYPE')
            .sort_values(by=['PERIOD']))
