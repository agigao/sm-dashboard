import cx_Oracle as cxo
from sqlalchemy import create_engine


def read_sql(script):
    """
    Well, at bit of weirdness is going on here, so to explain
    the function reads a script file which includes \n characters
    which obviously will break sql code, so we do get rid of 'em.
    :param script: script
    :return string: properly formatted SQL
    """
    try:
        return open(f'sql/{script}', 'r').read().replace("\n", ' ')
    except IOError:
        print("ERROR: File doesn't exist")


def query():
    """
    Setup database connection to Oracle and fetch data.
    :return data: a list of fetched data,
            columns: a list of columns.
    """

    # Database connection setup
    # build connection vars for oracle
    host, port, sid = ('fill', 'fill', 'fill')
    user, password = ('fill', 'fill')
    sid = cxo.makedsn(host, port, sid=sid)
    db = f'oracle://{user}:{password}@{sid}'

    # create SQLAlchemy engine instance
    engine = create_engine(db, echo=True)

    # build SQL
    sql = read_sql("script.sql")

    # open connection
    connection = engine.connect()

    # query/fetch data from db
    # IMPORTANT: only fetch all if data is reasonable size.
    data = connection.execute(sql).fetchall()

    # close connection
    connection.close()

    # get a list of columns and make em uppercase
    columns = list(map(str.upper, data[0].keys()))

    return data, columns
