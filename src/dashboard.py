from pandas import Timestamp
from bokeh.models import (Select, ColumnDataSource, NumeralTickFormatter,
                          Range1d, HoverTool, Span,
                          WidgetBox, Panel, DatePicker)
from bokeh.layouts import row, gridplot
from bokeh.plotting import figure
from bokeh.transform import linear_cmap


def dashboard(df):
    """
    So, what's going on here?
    In a nutshell, this is a script which does rendering a whole dashboard for Model Monitoring Web-App.
    Major function dashboard, which is fired from main.py
    :param df: data, which is never ought to be altered! Sorry, Py, Clojure got you here!
    :return tab: Dashboard Panel.
    """

    def make_dataset(data, category, model, version, target):
        """build the init data upon reading init values of all selector options:
        - category
        - model
        - version
        - target."""

        return data.loc[category] \
            .loc[model] \
            .query(f'VERSION_NAME == "{version}"') \
            .query(f'TARGET_TYPE == "{target}"')

    def style(plot):
        """Some styling, nothing fancy though."""
        plot.title.align = 'center'
        plot.title.text_font_size = '20pt'
        plot.title.text_font = 'sans'
        plot.background_fill_color = "#f5f5f5"
        plot.grid.grid_line_color = "white"
        plot.axis.axis_line_color = None

        return plot

    def add_spans(plot):
        """Add spans for PSI plot over 10% and 25% threshold"""
        # low 10% threshold
        plot.add_layout(Span(location=0.1, dimension='width', line_color='#91CF60', line_dash='dashed'))
        # high 25% threshold
        plot.add_layout(Span(location=0.25, dimension='width', line_color='#D7191C', line_dash='dashed'))

    def make_plot(src, metric):
        """
        rather long function, but it does it all per metric:
        - plot rendering
        - assigning x/y data
        -
        :param src: data
        :param metric: metric {GINI, PSI, KS, IV}
        :return plot:
        """

        # base color of the plot
        base = '#007be5'
        # color-mapper for PSI
        normal, low, high = ([base], '#91CF60', '#D7191C')

        # plot = figure(x_axis_type="datetime", x_axis_label='Month/Year', y_axis_label='Performance',
        #               title=f'{metric}', plot_height=550, plot_width=1040)

        plot = figure(x_axis_type="datetime", x_axis_label='Month/Year', y_axis_label='Performance',
                      title=f'{metric}', output_backend="webgl", tools='pan,wheel_zoom, save, reset',
                      plot_height=400, sizing_mode="stretch_both")

        plot.line(x='PERIOD', y=metric, alpha=0.8, line_width=2,
                  color=base, source=src, name='line')

        circle = plot.circle(x='PERIOD', y=metric, size=8,
                             color=base, source=src, name='circle')

        # add hover
        hover = HoverTool(tooltips=[(metric, f'@{metric}'), # {%0.2f}
                                    ('DATE', '@PERIOD{%m-%Y}')],
                          formatters={'PERIOD': 'datetime',
                                      metric: 'printf'})

        plot.add_tools(hover)

        # check which type of metric do we got here and alter the visuals of plot accordingly
        if metric is not 'IV':
            # % format for Y axis
            plot.yaxis.formatter = NumeralTickFormatter(format="0%")
            if metric in ['GINI', 'KS']:
                plot.y_range = Range1d(0, 1)
            if metric == 'PSI':
                # PSI: Linear ColorMap
                # Use the field name of the column source
                mapper = linear_cmap(field_name=metric, palette=normal,
                                     low=0.1, high=0.25,
                                     low_color=low, high_color=high)
                # add spans for 10% and 25%
                add_spans(plot)

                # add color mapper
                glyph = circle.glyph
                glyph.line_color = mapper
                glyph.fill_color = mapper
        else:
            plot.y_range = Range1d(start=0, end=max(df['IV']) + 0.1)

        # apply styles
        plot = style(plot)

        return plot

    def update_cat(attr, old, new):
        """
            Here we got all 4 selectors functions, all of them update options below her and filters data along the way.
            category: updates - model/version/target options and data
            model - version/target options and data
            version - target options and data
        """
        # get subset of data for the category
        subset = df.loc[category_menu.value]
        # get model options for the category and load 'em to model.options data
        model_menu.options = list(subset.index.unique())
        # set the first from updated options as a value of a selector
        model_menu.value = model_menu.options[0]

        # ---------------------------------------------------------------
        # The same goes for all here, so I won't repeat myself that much.
        # ---------------------------------------------------------------
        
        # update version options under the model
        subset = subset.loc[model_menu.value]
        # PD Here
        version_menu.options = list(subset['VERSION_NAME'].unique())
        version_menu.value = version_menu.options[0]

        # update target options under the model
        subset = subset.query(f'VERSION_NAME == "{version_menu.value}"')
        target_menu.options = list(subset['TARGET_TYPE'].unique())
        target_menu.value = target_menu.options[0]

        subset = subset.query(f'TARGET_TYPE == "{target_menu.value}"')
        source.data = dict(subset)

    def update_mod(attr, old, new):
        """The same as update_cat, but below one level"""
        subset = df.loc[category_menu.value]

        # update version options under the model
        subset = subset.loc[model_menu.value]
        version_menu.options = list(subset['VERSION_NAME'].unique())
        version_menu.value = version_menu.options[0]

        # update target options under the model
        subset = subset.query(f'VERSION_NAME == "{version_menu.value}"')
        target_menu.options = list(subset['TARGET_TYPE'].unique())
        target_menu.value = target_menu.options[0]

        subset = subset.query(f'TARGET_TYPE == "{target_menu.value}"')
        source.data = dict(subset)

    def update_ver(attr, old, new):
        """The same as update_mod but again, below one level of update_mod this time"""
        subset = df.loc[category_menu.value]
        # update version options under the model
        subset = subset.loc[model_menu.value]

        # update target options under the model
        subset = subset.query(f'VERSION_NAME == "{version_menu.value}"')
        target_menu.options = list(subset['TARGET_TYPE'].unique())
        target_menu.value = target_menu.options[0]

        subset = subset.query(f'TARGET_TYPE == "{target_menu.value}"')

        source.data = dict(subset)

    def update_tar(attr, old, new):
        """Performs the same operation, just one level below the function: update_ver"""
        subset = df.loc[category_menu.value]
        subset = subset.loc[model_menu.value]
        subset = subset.query(f'VERSION_NAME == "{version_menu.value}"')
        # final cut of the data
        subset = subset.query(f'TARGET_TYPE == "{target_menu.value}"')
        source.data = dict(subset)

    def update_start_date(attr, old, new):
        source.data = dict(subset[subset['PERIOD'] > Timestamp(new)])

    def update_end_date(attr, old, new):
        source.data = dict(subset[subset['PERIOD'] < Timestamp(new)])

    # -----------------------------------------------------------------------------------
    # Load options for the starting point
    categories = list(df.index.levels[0])
    models = list(df.loc[categories[0]].index.unique())
    versions = list(df.query(f'MODEL_NAME == "{models[0]}"')['VERSION_NAME'].unique())
    targets = list(df.query(f'VERSION_NAME == "{versions[0]}"')['TARGET_TYPE'].unique())

    # Initialize bokeh controllers, controller - SELECT in this very case.
    category_menu = Select(options=categories, value='Application', title='Category')
    model_menu = Select(options=models, value='Credit Cards', title='Model')
    version_menu = Select(options=versions, value='V2.4', title='Version')
    target_menu = Select(options=targets, value='Target', title='Target')

    # bind functions to menu changes
    category_menu.on_change('value', update_cat)
    model_menu.on_change('value', update_mod)
    version_menu.on_change('value', update_ver)
    target_menu.on_change('value', update_tar)
    # -----------------------------------------------------------------------------------

    # -----------------------------------------------------------------------------------
    # date pickers for range
    start_date = DatePicker(title="Start Date", value=min(df.PERIOD),
                            min_date=min(df.PERIOD), max_date=max(df.PERIOD))
    end_date = DatePicker(title="End Date", value=max(df.PERIOD),
                          min_date=min(df.PERIOD), max_date=max(df.PERIOD))

    start_date.on_change('value', update_start_date)
    end_date.on_change('value', update_end_date)
    # ------------------------------------------------------------------------------------

    # -----------------------------------------------------------------------------------
    # subset: local data
    subset = make_dataset(df, category_menu.value, model_menu.value,
                          version_menu.value, target_menu.value)

    # load init bokeh's native ColumnDataSource format data into source.
    source = ColumnDataSource(subset)

    # make all plot for all metrics
    # One suggestion here: when a language semantics doesn't play well with functional way of thinking
    # don't force it upon yourself. You'll avoid some trouble.
    GINI = make_plot(source, 'GINI')
    PSI = make_plot(source, 'PSI')
    IV = make_plot(source, 'IV')
    KS = make_plot(source, 'KS')

    # bind controllers together in a widgetbox
    controls = WidgetBox(category_menu, model_menu, version_menu, target_menu,
                         start_date, end_date)
    # create a row and column layouts (which might change later)
    grid = gridplot([GINI, KS, PSI, IV], ncols=2, toolbar_location='above')

    layout = row(controls, grid)

    # create a Model Monitoring Dashboard tab
    tab = Panel(child=layout, title='Model Monitoring Dashboard')
    # -----------------------------------------------------------------------------------
    return tab
