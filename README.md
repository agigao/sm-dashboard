# Stat-Model Monitoring System

This web app is ought to provide clear insight regarding the performance of Statistical Models.

## Metrics:
- `GINI` Index
- `IV`: Information Value
- `PSI`: Probability, Statistics & Information
- `KS`: Kolmogorov-Smirnov Statistic

## Screenshot
![Screenshot](shot.png)

## Miscellaneous details:
- Pulls the data from Oracle DB
- SQL source is slurped from script.sql in sql directory
- Builds a dashboard and is capable of doing more!
- Dashboard contains 4 plots for metrics: GINI, PSI, IV, KS, respectively.

## How to run?
- Windows: run.bat 
- MacOS/GNU-Linux: run.sh

## Address
- [http://localhost:8080](http://localhost:8080)

## Adapting for your own project?
Well, you have do some heavy alteration tasks in:
1. Structual details in `data.py` and `dashboard.py`
2. some database connection details in `sql.py`
3. Appropriate select in `script.sql`

Have fun!<br>
(c) Giga at Chokhe.li



