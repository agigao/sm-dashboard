from bokeh.models import Tabs, Div
from bokeh.io import curdoc as doc
from bokeh.themes import built_in_themes

import src.sql as sql
import src.data as d
import src.dashboard as dash

# query database and convert to df
# "*" does unpacking
df = d.to_dataframe(*sql.query())
# build dashboard tab
model_monitoring = dash.dashboard(df)
# tab demonstration
some_other = dash.dashboard(df)
some_other.title = "Placeholder for another tab and for another mission."
tabs = Tabs(tabs=[model_monitoring, some_other])
# render
doc().add_root(tabs)
# apply theme, dark_minimal is another one for different time and taste.
doc().theme = 'light_minimal'
